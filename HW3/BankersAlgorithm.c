#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
// Banker's Algorithm - Joseph Haaga

/* these may be any values >= 0 */ 
#define SIZE 100
#define THREAD_COUNT 5
#define SLEEP_TIME 1
#define NUMBER_OF_CUSTOMERS 5 
#define NUMBER_OF_RESOURCES 3

void *handler (void *params);

int k=0;
int threadCount = 0;
pthread_mutex_t mutex;

/* the available instances of each resource */
int available[NUMBER_OF_RESOURCES];
/*the maximum # of instances demanded by each process */
int maximum[NUMBER_OF_RESOURCES][NUMBER_OF_CUSTOMERS];
/* the amount currently allocated to each customer */ 
int allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
/* the remaining need of each customer */
int need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];

int request_resources(int customer_num, int request[]){
	return 0; // if successful
	return -1; // unsuccessful
}

int release_resources(int customer_num, int release[]){
	return 0; // if successful
	return -1; // unsuccessful
}

void *handler(void *params){
int safe = 1;
int request[NUMBER_OF_RESOURCES];
int release[NUMBER_OF_RESOURCES];
int work[NUMBER_OF_RESOURCES];

//  request resources (run Bankers Alg here)
//	sleep for 1 second
//	telease resources

tryItAgain:	
	pthread_mutex_lock(&mutex);
	int temp_available[NUMBER_OF_RESOURCES];
	 threadCount++;
    printf("Thread %d started\n", threadCount);
	/* the amount currently allocated to each customer */ 
	int temp_allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
	int temp_need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
	int finish[NUMBER_OF_CUSTOMERS] = { 0 };
	// printf("\tThread %d vars initialized...\n", threadCount);
// resourceRequest(){
	// generate random Request[i] vector bounded by Need[i]
	printf("need[threadCount][p] = %d\n",need[threadCount-1][0]);
	printf("P%d is requesting :[",threadCount);
	for(int p=0;p<NUMBER_OF_RESOURCES;p++){
		request[p]= rand() % (need[threadCount-1][p]);
		printf(" %d",request[p]);
	}
	printf("]\n");

	// CHECK REQUEST CAN BE GRANTED
	// 	check Request[i]<Available;
	for(int q=0;q<NUMBER_OF_RESOURCES;q++){
		if(request[q]>available[q]){
			goto waitAWhile;
		}else{
			work[q]=available[q]+request[q];
		}
	}

	//  Have the system pretend to have allocated the requested resources to process Pi 
	/*
	Available = Available – Request[i] ;
	Allocationi =Allocation[i] +Request[i];
	Needi =Need[i]–Request[i];
	*/

	//Available = Available – Request[i] ;
	printf("\ntesting\n");
	printf("\ttemp_available:");
	for(int i=1; i<=NUMBER_OF_RESOURCES; i++){
		temp_available[i-1]=available[i-1]-request[i-1];
		printf("%d\t",temp_available[i-1]);
	}
	printf("\n\n");
	printf("\ttemp_allocation:");

	//Allocationi =Allocation[i] +Request[i];
	for(int i=0; i<NUMBER_OF_CUSTOMERS;i++){	
		for(int k=0; k<NUMBER_OF_RESOURCES;k++){
			if(i==threadCount-1){
				temp_allocation[i][k] = allocation[i][k] + request[k];
			}else{
				temp_allocation[i][k] = allocation[i][k];
			}
			printf("%d\t",temp_allocation[i][k]);
		}
		printf("\n\t\t\t");
	}


	printf("\n\n");
	printf("\ttemp_need:\t");

	//Needi =Need[i]–Request[i];
	memcpy(&temp_need,&need,sizeof need);
	for(int j=0;j<NUMBER_OF_CUSTOMERS;j++){
		for(int k=0;k<NUMBER_OF_RESOURCES;k++){
			if(j==threadCount-1){
				temp_need[j][k] = need[j][k] - request[k];	
			}
			// temp_need[j][k] = need[j][k] - temp_allocation[k][j];
			printf("%d\t",temp_need[j][k]);
		}
		printf("\n\t\t\t");
	}

	printf("\nSafeState Check;\n");
	// printf("tempNeed: %d\n",*temp_need[0]);

	// Check Safety of State of temp_allocation, temp_need, and temp_available
	// 1. work=available and finish = {0, 0, 0, .....}
	memcpy(&work,&temp_available, sizeof work);
	// printf("Memcpy worked");
	// 2. Find an index i such that both a. Finish[i] == false AND b. Needi ≤ Work 
	printf("\tsafe state solution:\t");
	for(int i=0;i<NUMBER_OF_CUSTOMERS;i++){
		steptwo:
		// 	check Need[i]<Work;
		for(int q=0;q<NUMBER_OF_RESOURCES;q++){
			if(*temp_need[q]>work[q]){
				//FAIL
				// printf("%d  > %d\n",*temp_need[q],work[q]);
			}
		}
		if(finish[i]==0){
			work[i] = *(work[i] + temp_allocation[i]);
			printf(" P%d ",i+1);
			// 3. Work = Work + Allocationi
			finish[i] = 1;
			// Finish[i] = 1
			// goto steptwo;
			// Go to step 2.	
		}
	}
	// if(num_i_found==0){}
	for(int i=0; i<NUMBER_OF_CUSTOMERS;i++){
		if(finish[i]==0){
			safe = 0;
			printf("\tP%d leaves system in unsafe state!\n",threadCount);
		}
	}

	// commit changes to Available, Allocated, Need and Max if the resulting state is safe
	if(safe){
		// printf("\tProcess %d approved\n",threadCount);
		memcpy(&available, &temp_available, sizeof available);
		// printf("\tAvailable copied\n");
		memcpy(&allocation, &temp_allocation, sizeof allocation);
		// printf("\tAllocation copied\n");
		memcpy(&need, &temp_need, sizeof need);
		// printf("\tNeed copied\n");

		printf("\nP%d is releasing :[",threadCount);
		for(int p=0;p<NUMBER_OF_RESOURCES;p++){
			release[p]= rand() % (request[p] || 1);
			printf(" %d",release[p]);
		}
		printf("]\n");

		for(int k=0; k<NUMBER_OF_RESOURCES;k++){
			
			allocation[threadCount-1][k] = allocation[threadCount-1][k] - release[k];
			printf("%d\t",allocation[threadCount-1][k]);
		}

		
	}else{
		printf("\tProcess declined; UNSAFE STATE!\n");
	}


    sleep(SLEEP_TIME);

    printf("Thread %d done\n", threadCount);
    printf("\n\n");

    pthread_mutex_unlock(&mutex);

    pthread_exit(0);

    // request not completed for some reason. waiting a cycle
waitAWhile:

	pthread_mutex_unlock(&mutex);
	sleep(SLEEP_TIME);
	goto tryItAgain;
}

int main(int argc, char *argv[]){
	pthread_t threadIDs[THREAD_COUNT];
    pthread_attr_t attr;
	int i;
	int temp;
	int count = NUMBER_OF_CUSTOMERS;
	// occupy Available from command line args
	printf("\n");
	printf("Available Resources:\n");
	for(int i=1; i<argc; i++){
		available[i-1]=atoi(argv[i]);
		printf("%d\t",available[i-1]);
	}
	printf("\n");
	printf("\n");
	printf("Maximum Resources by Process:\n");

	printf("\n");


	// initialize Max however I want
	for(int j=0;j<NUMBER_OF_RESOURCES;j++){
		temp = floor(available[j]/NUMBER_OF_CUSTOMERS);
		// evenly distribute instances of each resource across all processes
		for(int k=0;k<NUMBER_OF_CUSTOMERS;k++){
			maximum[j][k] = temp;
			// put remainder in last processes
			if(k==NUMBER_OF_CUSTOMERS-1){
				maximum[j][k] = available[j]-(temp*(NUMBER_OF_CUSTOMERS-1));
			}
		}
		// printf("\n");
	}
	// print Max
	for(int y=0; y<NUMBER_OF_CUSTOMERS;y++){	
		for(int k=0; k<NUMBER_OF_RESOURCES;k++){
			printf("%d\t",maximum[k][y]);
		}
		printf("\n");
	}


	printf("\n");
	printf("\n");
	printf("Current Allocation by Process:\n");

	printf("\n");

	// initialize Allocation with 0's
	for(int j=0;j<NUMBER_OF_RESOURCES;j++){
		for(int k=0;k<NUMBER_OF_CUSTOMERS;k++){
			allocation[k][j] = 0;
		}
	}

	// print Allocation
	for(int i=0; i<NUMBER_OF_CUSTOMERS;i++){	
		for(int k=0; k<NUMBER_OF_RESOURCES;k++){
			printf("%d\t",allocation[k][i]);
		}
		printf("\n");
	}

	printf("\n");
	printf("\n");
	printf("Need by Process:\n");

	printf("\n");

	// initialize Need as Max-Allocation
	for(int j=0;j<NUMBER_OF_RESOURCES;j++){
		for(int k=0;k<NUMBER_OF_CUSTOMERS;k++){
			need[k][j] = maximum[j][k] - allocation[k][j];
		}
	}

		// print Allocation
	for(int i=0; i<NUMBER_OF_CUSTOMERS;i++){	
		for(int k=0; k<NUMBER_OF_RESOURCES;k++){
			printf("%d\t",need[i][k]);
		}
		printf("\n");
	}

	printf("\n");
	printf("\n");
	printf("\n");

    pthread_attr_init(&attr);
    pthread_mutex_init(&mutex, NULL);

    while(k<4){
	    for(i = 0; i < THREAD_COUNT; i ++)
	    	// printf("Thread created\n");
	        pthread_create(&threadIDs[i], &attr, handler, NULL);
	    

	    for(i = 0; i < THREAD_COUNT; i ++)
	        pthread_join(threadIDs[i], NULL);
	    k++;
	    threadCount=0;
	}

    printf("All threads complete!\n");
    pthread_mutex_destroy(&mutex);


}


