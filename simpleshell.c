/* Joseph Haaga's Shell for GWU CS Operating Systems HW2 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#define MAXLINE 80 /* The maximum length command */
int historyCount=0;
int doneReading=0;

struct historyItem{
	int cmd_num;
	char* command;
	struct historyItem* next;
};

struct historyFeature{
	struct historyItem* head;
};


struct historyItem *createHistoryItem(char* cmd){
	struct historyItem *node = malloc(sizeof(struct historyItem));
    node->cmd_num=historyCount;
    historyCount++;
    node->command=cmd;
    node->next=NULL;
    return node;
};


// splits one string of parameters into an array of parameters
void parseCmd(char* cmd,char** args){
	int i;
	// for each substring in cmd string
    for(i = 0; i < (MAXLINE/2 + 1); i++) {
        // split at next whitespace char
        args[i] = strsep(&cmd, " "); 
        // exit case - NULL remaining
        if(args[i] == NULL) {
        	break;
        }
    }
}

// execution subroutine; creates child process to execute command
int executeCmd(char** args){
	pid_t pid = fork();
	if(pid > 0) { // parent
		int child; // hold child status
    	waitpid(pid, &child, 0); // wait for child
    	return 0;
    }else if(pid == 0) { // child
    	execvp(args[0], args); // execute command
    	return 0;
    } else { // error
    	return 1;
    }
}

// main loop to run Shell
int main(void){
	// buffers for reading user input
	char msg[50];
	struct historyItem *temp;
	char *args[MAXLINE/2 + 1]; 
	int should_run = 1; /* flag to determine when to exit program */
	struct historyFeature *hf = malloc(sizeof(struct historyFeature));
	struct historyItem *item = malloc(sizeof(struct historyItem));

    // ['first']
    item = hf->head;

	while (should_run) { 
		printf("osh debug>"); fflush(stdout);
		// get input from keyboard i/o
		fgets(msg, sizeof(msg), stdin);
	    // clean up input (remove newline char)
	    if(msg[strlen(msg) - 1] == '\n') { 
				msg[strlen(msg) - 1] = '\0'; 
		} 	
		// HISTORY: ['first']

		// NEW ITEM: ['msg']
		// 
		if(strcmp(msg, "!!")==0){
			while(historyCount>0){
				printf("\n%s\n",item->command);
				item=item->next;
				historyCount--;
			}
		}else{
		    // parse input into an array of parameters
		    parseCmd(msg, args);
		    // loop control determined by execute method's return status
		    should_run = executeCmd(args);

		    /* 	HISTORY FEATURE LOGIC */
		    // create new history feature item of user input
			item = createHistoryItem(msg);		// history item of latest input  [msg]
			/* case 1, first item in historyFeature */
			item->next=hf->head;
			hf->head=item;
			printf("\nhistory item MSG:%s\n",item->command);
		}
			
		/**
		* After reading user input, the steps are:
		* (1) fork a child process using fork()
		* (2) the child process will invoke execvp()
		* (3) if command included &, parent will invoke wait() */

		// handle quitters
		should_run = strcmp(args[0], "quit");
		

	}
	return 0;
}
