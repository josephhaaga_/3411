#include <stdio.h>
#include <math.h>
// virtualMemoryManager.c
// Joseph Haaga

#define PAGES 256;
#define FRAMES 256;
#define FRAME_SIZE 256;

struct page_entry {
	char frame;
	int mapped;
};

// int tlb[16][2];
typedef unsigned char byte;
byte physMem[65536];
struct page_entry pageTable[256]={0};
int tempPage;
int tempOffset;
char currentFrame = 0;


void initPageTable(){
	int z;
	int y;
	for(z=0; z<256;z++){	
		// pageTable[z]=-1;		
	}
}

void getPageNumber(int n){
	tempPage = 0;
	tempOffset = 0;
	int copyOne = 0; // becomes page #
	int copyTwo = 0; // becomes offset
	copyOne = n << 8;
	// printf("copyOne << 8 = %d\n",copyOne);
	copyOne = copyOne >> 16;
	// printf("copyOne >> 16 = %d\n",copyOne);
	copyTwo = n & 255;
	// printf("copyTwo & 255 = %d\n",copyTwo);
	tempPage = copyOne;
	tempOffset = copyTwo;
	// printf("\nPage:%d, Offset:%d",copyOne,copyTwo);
}

int reverseInt(int n, int len){
	int k=0;
	int done=0;

	// n = 06772 
	while(len>0){
			// k = 2
			// k = 27
		 	// k = 277
			// k = 2776
	         k=(k+(n%10));
	        // n = 0677
	        // n = 067
	        // n = 06
	         n /= 10;
	         if(n==0){
	         	done=1;
	         }
	         // k = 20
	         //  
	         k=k*10;
	         len--;
	         
         }
         return k/10;
}

void getPhysicalAddress(int page,int offset){
	// physMem[]
	printf("Physical Address:%d\n",(currentFrame*256)+offset);
	pageTable[tempPage].frame = currentFrame;
	// printf("\tpageTable[tempPage] = pT[%d] = %d\n",tempPage,pageTable[tempPage]);
	currentFrame++;
}


int main(int argc, char **argv){
	unsigned char *buffer[16];
	char n;
	unsigned char *buffer2[16];

// read input file
	int counter=20000;
	FILE *input_reader;
	FILE *backing_store;
	input_reader=fopen(argv[1],"rb");
	backing_store=fopen("backing_store.bin","rb");
	int i=0;
	int j=0;

	initPageTable();
	printf("physMem[758]=%d\n",physMem[758]);
	printf("pageTable[1]=%d\n",pageTable[1]);
	int builder=0;
	while(counter>0){
		n = fgetc(input_reader);
		if(n=='\n'){
	// calculate page and offset
			printf("\nVirtual Address:%d\t",reverseInt(builder,i));
			getPageNumber(reverseInt(builder,i));
			
		//  check page table to see if page is in memory
			if(pageTable[tempPage].mapped == 1){
				printf("Physical Address:%d",(currentFrame*256)+tempOffset);
				// printf("%d\t Value: %d\n",pageTable[tempPage].frame+tempOffset,physMem[256*currentFrame+tempOffset]);
				printf("\t Value: %d\n",physMem[pageTable[tempPage].frame]);
				// printf("\tpageTable[tempPage] = pT[%d] = %d\n",tempPage,pageTable[tempPage]+tempOffset);
				// getPhysicalAddress(tempPage,tempOffset);
			}else{
				// printf("\tnot in memory :(\n");
				getPhysicalAddress(tempPage,tempOffset);

				fseek(backing_store, 256*currentFrame, 0);

				for(j=0;j<256;j++){
					fread(&physMem[256*currentFrame+i],sizeof (char),1, backing_store);	
				}
				// for int i 0 -> 256
				printf("tlb miss->Value:%d\t",physMem[256*currentFrame+tempOffset]);

				pageTable[tempPage].mapped=1;
				pageTable[tempPage].frame = currentFrame;
			}

			

			tempPage=0;
			tempOffset=0;
			i=0;
			builder = 0;
		}else{
			counter--;
			builder = builder + pow(10,i)*(n-'0');
 			// fprintf(stdout, "%d",n-'0');
 			i++;
		}
	// }
	// printf("\nPhysmem\n");
	// for(int i=0;i<6000;i++){
	// 	printf(" %d",physMem[i]);
	}

	// check page table


}