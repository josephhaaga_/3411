import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
// Joseph Haaga

public class DiskScheduling{

	int FCFS(int[] i, int startingIndex){
		int total = 0;
		int currentIndex=startingIndex;
		// sequential order
		for(int k=0; k<i.length; k++){
			total+=Math.abs(i[k] - currentIndex);
			currentIndex = i[k];
		}
		return total;
	}



	int runFCFS(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=FCFS(j,startingIndex);
		}
		return total/10;
	}


	int SSTF(int[] i, int startingIndex){
		// While queue has unvisited positions
		// for each unvisited position
		//		calculate absVal(distance to currentPosition)
		// set currentPosition to min of calculated distances
		// remove currentPosition from queue 

		int currentPosition = startingIndex;
		List<Integer> z = new ArrayList<Integer>();
		for(int k=0;k<i.length;k++){
			z.add(i[k]);
		}
		int total = 0;
		int p=0;
		// While queue has unvisited positions
		while(p<z.size()){
			int[] distances = new int[z.size()];
			// // for each unvisited position
			// for(int j=0;j<z.size();j++){
			// 	// System.out.print(z.get(j)+" ");
			// }
			int min=10000;
			int minIndex =0;
			// System.out.println();
			for(int j=0;j<z.size();j++){
				//calculate absVal(distance to currentPosition)
				distances[j] = Math.abs(currentPosition-z.get(j));
				// System.out.print(distances[j]+" ");
				if(distances[j]<min){
					min = distances[j];
					minIndex = j;
				}
			}
			// System.out.println("\n"+min);
			total+=min;
			currentPosition = z.get(minIndex);
			z.remove(minIndex);
			// System.out.println();
		}
		return total;	
	}

	int runSSTF(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		int[] distances = new int[i.length];
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=SSTF(j,startingIndex);
		}
		return total/10;
	}

	int SCAN(int[] i, int startingIndex){
		int currentPosition = startingIndex;
		int[] distances = new int[i.length];
		int total = 0;
		Arrays.sort(i);
		int place = 0;
		//System.out.println();
		for(int j=0;j<i.length;j++){
			distances[j] = Math.abs(i[j]-currentPosition);
		//	System.out.print(distances[j]+" ");
			if(i[j]<currentPosition){
				place = j;
			}

		}
		if(place<i.length/2){
			// go right first
			total+=(5000-startingIndex);
			// service last request at first disk sector
			total+=(5000-i[0]);
		}
		else{
			// go left first
			total+=(startingIndex+i[i.length-1]);
		}

		//System.out.println();
		return total;
	}

	int runSCAN(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=SCAN(j,startingIndex);
		}
		return total/10;
	}


	
	int CSCAN(int[] i, int startingIndex){
		int currentPosition = startingIndex;
		int[] distances = new int[i.length];
		int total = 0;
		Arrays.sort(i);
		int place = 0;
		//System.out.println();
		for(int j=0;j<i.length;j++){
			distances[j] = Math.abs(i[j]-currentPosition);
		//	System.out.print(distances[j]+" ");
			if(i[j]<currentPosition){
				place = j;
			}

		}
		if(place<i.length/2){
			// go right first
			total+=(5000-startingIndex);
			// service last request at disk sector to immediate left of startingPosition
			total+=(i[place]);
		}
		else{
			// go left first
			total+=(startingIndex+i[place+1]);
		}

		//System.out.println();
		return total;
	}


	int runCSCAN(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=CSCAN(j,startingIndex);
		}
		return total/10;
	}


	int LOOK(int[] i, int startingIndex){
		int currentPosition = startingIndex;
		int[] distances = new int[i.length];
		int total = 0;
		Arrays.sort(i);
		int place = 0;
		//System.out.println();
		for(int j=0;j<i.length;j++){
			distances[j] = Math.abs(i[j]-currentPosition);
		//	System.out.print(distances[j]+" ");
			if(i[j]<currentPosition){
				place = j;
			}

		}
		if(place<i.length/2){
			// go right first
			total+=(i[i.length-1]-startingIndex);
			// service last request at first disk sector
			total+=(i[i.length-1]-i[0]);
		}
		else{
			// go left first
			total+=(startingIndex-i[0]);
			total+=(i[i.length-1]);
		}

		//System.out.println();
		return total;
	}


	int runLOOK(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=LOOK(j,startingIndex);
		}
		return total/10;
	}


	
	int CLOOK(int[] i, int startingIndex){
		int currentPosition = startingIndex;
		int[] distances = new int[i.length];
		int total = 0;
		Arrays.sort(i);
		int place = 0;
		//System.out.println();
		for(int j=0;j<i.length;j++){
			distances[j] = Math.abs(i[j]-currentPosition);
		//	System.out.print(distances[j]+" ");
			if(i[j]<currentPosition){
				place = j;
			}

		}
		if(place<i.length/2){
			// go right first
			total+=(i[i.length-1]-startingIndex);
			// service last request at first disk sector
			total+=(i[place]-i[0]);
		}
		else{
			// go left first
			total+=(startingIndex-i[0]);
			total+=(i[i.length-1]-i[place]);
		}

		//System.out.println();
		return total;
	}


	int runCLOOK(int[] i, int startingIndex){
		int numberOfRuns = 0;
		int total = 0;
		while(numberOfRuns<10){
			int[] j = Arrays.copyOfRange(i,(numberOfRuns*100),((numberOfRuns+1)*100)-1);
			numberOfRuns++;
			total+=CLOOK(j,startingIndex);
		}
		return total/10;
	}




	// Instead of having each algorithm look at the entire 1000 requests at once,
	// restrict them to a “moving window” of 100 requests at a time.

	// Your program will service a disk with
	// 5,000 cylinders numbered 0 to 4,999. 
	// The program will generate a random series 
	// of 1,000 cylinder requests and service them 
	// according to each of the algorithms listed above.
	// The program will be passed the initial position of
	// the disk head (as a parameter on the command line)
	// and report the total amount of head movement required
	// by each algorithm.

	public static void main(String args[]){
		int[] p = new int[8];
		p[0] = 98;
		p[1] = 183;
		p[2] = 37;
		p[3] = 122;
		p[4] = 14;
		p[5] = 124;
		p[6] = 65;
		p[7] = 67;
		int st= 53;

		DiskScheduling d = new DiskScheduling();
		// int q = d.FCFS(p,st);
		// System.out.println("FCFS result:"+q+" cylinders");

		int[] s = new int[8];
		s[0] = 98;
		s[1] = 183;
		s[2] = 37;
		s[3] = 122;
		s[4] = 14;
		s[5] = 124;
		s[6] = 65;
		s[7] = 67;
		// s = {98,183,37,122,14,124,65,67};

		
		// int r = d.SSTF(s,st);
		// System.out.println("SSTF result:"+r+" cylinders");

		/////////////////// GENERATE RANDOM NUMBERS ////////////////
		int[] a = new int[1000];
		int aCounter = 0;
		while(aCounter<a.length){
			a[aCounter]=(int)(Math.random()*5000);
			// System.out.print(a[aCounter]+" ");
			aCounter++;
		}
		int fcfsTotal = d.runFCFS(a,Integer.parseInt(args[0]));
		System.out.println("FCFS Total:"+fcfsTotal);
		int sstfTOTAL = d.runSSTF(a,Integer.parseInt(args[0]));
		System.out.println("SSTF Total:"+sstfTOTAL);
		int scanTotal = d.runSCAN(a,Integer.parseInt(args[0]));
		System.out.println("SCAN Total:"+scanTotal);
		int cscanTotal = d.CSCAN(a,Integer.parseInt(args[0]));
		System.out.println("CSCAN Total:"+cscanTotal);
		int lookTotal = d.LOOK(a,Integer.parseInt(args[0]));
		System.out.println("LOOK Total:"+lookTotal);
		int clookTotal = d.CLOOK(a,Integer.parseInt(args[0]));
		System.out.println("CLOOK Total:"+clookTotal);
 
	}
}
